eclipse-platform-debug (4.26-1+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:12:21 +0530

eclipse-platform-debug (4.26-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 16:22:55 +0000

eclipse-platform-debug (4.26-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-core-resources-java (>= 3.18)
    - Build more bundles with Java 11
  * Standards-Version updated to 4.6.2

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 04 Jan 2023 00:12:28 +0100

eclipse-platform-debug (4.24-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jface-text-java (>= 3.20)
    - Build more bundles with Java 11
  * Standards-Version updated to 4.6.1
  * Track and download the new releases from GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 09 Dec 2022 09:19:56 +0100

eclipse-platform-debug (4.21-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.6.0.1
  * Track the new releases from git.eclipse.org (the GitHub repository is gone)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 18 Oct 2021 10:05:42 +0200

eclipse-platform-debug (4.18-1+apertis1) apertis; urgency=medium

  * Set component to sdk.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 06 Jan 2022 17:26:58 +0530

eclipse-platform-debug (4.18-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 16:57:03 +0000

eclipse-platform-debug (4.18-1) unstable; urgency=medium

  * New upstream release
    - Depend on Java 11
    - New dependency on libeclipse-e4-ui-services-java
  * Standards-Version updated to 4.5.1

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 28 Dec 2020 11:12:44 +0100

eclipse-platform-debug (4.17-1) unstable; urgency=medium

  * New upstream release
    - Updated the bundle dependencies
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 02 Oct 2020 09:37:43 +0200

eclipse-platform-debug (4.16-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 24 Aug 2020 00:16:02 +0200

eclipse-platform-debug (4.15-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-text-java (>= 3.9)
  * Standards-Version updated to 4.5.0
  * Switch to debhelper level 12

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 07 May 2020 23:29:16 +0200

eclipse-platform-debug (4.12-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-ui-workbench-java (>= 3.115)
  * Standards-Version updated to 4.4.0

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 18 Jul 2019 11:00:44 +0200

eclipse-platform-debug (4.11-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 11 Jul 2019 11:09:55 +0200

eclipse-platform-debug (4.10-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 23:26:12 +0000

eclipse-platform-debug (4.10-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-ui-java (>= 3.111)
  * Standards-Version updated to 4.3.0

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 28 Dec 2018 15:47:14 +0100

eclipse-platform-debug (4.9-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-ui-java (>= 3.110)
    - The license changed to EPL-2.0

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 04 Dec 2018 16:36:56 +0100

eclipse-platform-debug (4.8-1) unstable; urgency=medium

  * New upstream release
    - Depend on libswt-gtk-4-java (>= 4.8)
    - Added equinox-preferences to the classpath of org.eclipse.ui.console

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 28 Nov 2018 00:25:26 +0100

eclipse-platform-debug (4.7.3-1) unstable; urgency=medium

  * Initial release (Closes: #909610)

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 26 Oct 2018 10:40:38 +0200
